#!/usr/bin/env python3
import argparse
import requests
from datetime import datetime, timedelta
import os
import json
from appdirs import user_data_dir

conf = {
    'CITY_ID': "2643743",
    'API_KEY': "fillmein",
    'API_CUR': "https://api.openweathermap.org/data/2.5/weather?id={CITY_ID}&appid={API_KEY}&units=metric",
    'API_FUT': "https://api.openweathermap.org/data/2.5/forecast?id={CITY_ID}&appid={API_KEY}&units=metric"
}

def group_to_info(group_id):
    # https://openweathermap.org/weather-conditions
    msd = int(str(group_id)[0]) # most significant digit
    if group_id == 800:
        return ["Clear",""]
    elif msd == 8:
        return ["Cloudy",""]
    elif msd == 7:
        return ["Fog",""]
    elif msd == 6:
        return ["Snow",""]
    elif msd == 5:
        return ["Rainy",""]
    elif msd == 3:
        return ["Rainy",""]
    elif msd == 2:
        return ["Storm",""]
    else:
        return ["Misc",""]

def parse_current(raw, timeformat):
    result = {}
    desc,icon = group_to_info(raw['weather'][0]["id"])
    result['icon'] = icon
    result['desc'] = desc
    result['temp'] = raw['main']['temp']
    result['humi'] = raw['main']['humidity']
    result['sset'] = datetime.fromtimestamp(raw['sys']['sunset']).strftime(timeformat)
    result['wind'] = raw['wind']["speed"]
    return result

def parse_forecast(raw, timeformat):
    result = {}
    for data_idx, data in enumerate(raw['list']):
        suffix      = str(data_idx)
        desc,icon   = group_to_info(data['weather'][0]['id'])
        result['time'+suffix] = datetime.fromtimestamp(data['dt']).strftime(timeformat)
        result['icon'+suffix] = icon
        result['desc'+suffix] = desc
        result['temp'+suffix] = data['main']['temp']
        result['humi'+suffix] = data['main']['humidity']
        result['wind'+suffix] = data['wind']['speed']
    return result

def query_or_reload(url, type_of_req):
    USER_DIR = user_data_dir("rainy", "rainy_author")
    if not os.path.exists(USER_DIR):
        os.makedirs(USER_DIR)

    now         = datetime.now()
    file_path   = None
    for filename in os.listdir(USER_DIR):
        full_path = os.path.join(USER_DIR,filename)
        if os.path.isfile(full_path) and type_of_req in filename and len(filename.split('.')) == 2:
            time_created    = datetime.fromtimestamp(int(filename.split('.')[1]))
            age             = now-time_created
            if age > timedelta(minutes=30):
                os.remove(full_path)
            else:
                file_path = full_path

    if not file_path:
        # download
        api_raw         = requests.get(url).json()
        new_file_name   = "{}.{:.0f}".format(type_of_req, now.timestamp()-1)
        try:
            with open(os.path.join(USER_DIR, new_file_name), 'x') as new_file:
                new_file.write(json.dumps(api_raw, sort_keys=True, indent=4))
        except FileExistsError as e:
            # some other thread may have created the file
            pass
        except Exception as e:
            # something unexpected maybe
            print("Rainy error: "+str(e))
    else:
        # reload from disk
        with open(file_path) as data_file:
            api_raw = json.load(data_file)

    return api_raw


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get configurable weather info from https://openweathermap.org/')
    parser.add_argument('template_string', metavar='str', type=str, nargs='?',
                        help='the template string (default: \"{icon}  {desc} {temp}°C\" for current weather)')
    parser.add_argument('--forecast', action='store_true',
                        help='get a three day forecast (default: get current weather)')
    parser.add_argument('--timeformat', action='store',
                        help='use a specific (python-style) time format (default: %%H:%%M for current weather)')
    args = parser.parse_args()

    if args.forecast:
        if not args.template_string:
            args.template_string = ("{time0} {icon0}  {desc0:<6} {temp0:4.1f}°C\n"
                                    "{time6} {icon6}  {desc6:<6} {temp6:4.1f}°C\n"
                                    "{time8} {icon8}  {desc8:<6} {temp8:4.1f}°C")
            # args.template_string = ("{time0:^13}{time4:^13}{time8:^13}\n"
            #                         "{temp0:>9.1f}°C {temp4:>10.1f}°C {temp8:>10.1f}°C")
        if not args.timeformat:
            args.timeformat = "%a %H:%M"
        api_raw     = query_or_reload(conf['API_FUT'].format_map(conf), "forecast")
        api_result  = parse_forecast(api_raw, args.timeformat)
    else:
        if not args.template_string:
            args.template_string = "{icon}  {desc} {temp}°C"
        if not args.timeformat:
            args.timeformat = "%H:%M"
        api_raw     = query_or_reload(conf['API_CUR'].format_map(conf), "current")
        api_result  = parse_current(api_raw, args.timeformat)

    # mini language:
    #   "{icon}": weather icon
    #   "{desc}": weather description
    #   "{temp}": temperature in celsius
    #   "{humi}": humidity in percent
    #   "{sset}": sunset (only in current weather mode)
    #   "{wind}": wind speed in meters per second
    # for forecast:
    #   "{<icon/desc/temp/humi/wind/time>t}": show t-th data point in the future, e.g.:
    #   "{temp1}"  : temperature in celsius in 3 hours (data is available in 3 hour intervals)
    #   "{time3}"  : show exact time of predicted weather (not exactly now+9 hours)
    #   there are 40 data points available (5 days)

    print(args.template_string.format_map(api_result))


